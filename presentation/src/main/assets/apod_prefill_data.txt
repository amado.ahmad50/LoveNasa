[

{
  "copyright": "Robert Gendler",
  "date": "2016-02-26",
  "explanation": "The Tarantula Nebula is more than a thousand light-years in diameter, a giant star forming region within nearby satellite galaxy the Large Magellanic Cloud, about 180 thousand light-years away. The largest, most violent star forming region known in the whole Local Group of galaxies, the cosmic arachnid sprawls across this spectacular composite view constructed with space- and ground-based image data. Within the Tarantula (NGC 2070), intense radiation, stellar winds and supernova shocks from the central young cluster of massive stars, cataloged as R136, energize the nebular glow and shape the spidery filaments. Around the Tarantula are other star forming regions with young star clusters, filaments, and blown-out bubble-shaped clouds In fact, the frame includes the site of the closest supernova in modern times, SN 1987A, at the lower right. The rich field of view spans about 1 degree or 2 full moons, in the southern constellation Dorado. But were the Tarantula Nebula closer, say 1,500 light-years distant like the local star forming Orion Nebula, it would take up half the sky.",
  "hdurl": "http://apod.nasa.gov/apod/image/1602/Tarantula-HST-ESO-M.jpg",
  "media_type": "image",
  "service_version": "v1",
  "title": "The Tarantula Nebula",
  "url": "http://apod.nasa.gov/apod/image/1602/Tarantula-HST-ESO-SS1024.jpg"
},

{
  "copyright": "Jeff Dai",
  "date": "2016-02-25",
  "explanation": "Fans of planet Earth probably recognize its highest mountain, the Himalayan Mount Everest, on the left in this 3-panel skyscape of The World at Night. Shrouded in cloud Everest's peak is at 8,848 meters (29,029 feet) elevation above sea level. In the middle panel, stars trail above volcanic Mauna Kea forming part of the island of Hawaii. Festooned with astronomical observatories, its summit lies a mere 4,168 meters above sea level. Still, measured from its base starting below the ocean's surface, Mauna Kea is over 10,000 meters tall, making it Earth's tallest mountain from base to summit. At right, beneath the arc of the Milky Way is the Andean mountain Chimborazo in Ecuador. The highest equatorial mountain, the Chimborazo volcano's peak elevation is 6,268 meters above sea level. But rotating planet Earth is a flattened sphere (oblate spheroid) in shape, its equatorial diameter greater than its diameter measured pole to pole. Sitting nearly on top of Earth's greatest equatorial bulge, Chimborazo's peak is the farthest point on the planet's surface from the center, over 2,000 meters farther from the center of the Earth than Everest's peak. That makes Chimborazo's summit the place on Earth's surface closest to the stars.",
  "hdurl": "http://apod.nasa.gov/apod/image/1602/Everest-MaunaKea-Chimborazo-1800x600-cp8.jpg",
  "media_type": "image",
  "service_version": "v1",
  "title": "Highest, Tallest, and Closest to the Stars",
  "url": "http://apod.nasa.gov/apod/image/1602/Everest-MaunaKea-Chimborazo-1800x600-cp8.jpg"
},

{
  "date": "2016-02-24",
  "explanation": "Can you identify a familiar area in the northeast USA just from nighttime lights?  It might be possible because many major cities are visible, including (right to left) New York, Philadelphia, Baltimore, Washington, Richmond and Norfolk --  Boston of the USA's Northeast megalopolis is not pictured.  The featured image was taken in 2012 from the International Space Station.  In the foreground are two Russian cargo ships with prominent solar panels.  This Northeast megalopolis of the USA contains almost 20 percent of the people of the USA but only about 2 percent of the land area.  Also known also as the Northeast Corridor and part of the Eastern Seaboard, about 10 percent of the world's largest companies are headquartered here. The near continuity of the lights seem to add credence to the 1960s-era prediction that the entire stretch is evolving into one continuous city.",
  "hdurl": "http://apod.nasa.gov/apod/image/1602/NorthEastUSA_ISS_4256.jpg",
  "media_type": "image",
  "service_version": "v1",
  "title": "USA's Northeast Megalopolis from Space",
  "url": "http://apod.nasa.gov/apod/image/1602/NorthEastUSA_ISS_960.jpg"
},
{
  "date": "2016-02-23",
  "explanation": "Telescopes around the world are tracking a bright supernova that occurred in a nearby dusty galaxy.  The powerful stellar explosion was first noted earlier this month. The nearby galaxy is the photogenic Centaurus A, visible with binoculars and known for impressive filaments of light-absorbing dust that cross its center.  Cen A is featured here in a high-resolution archival Hubble Space Telescope image, with an inset image featuring the supernova taken from the ground only two days after discovery. Designated SN2016adj, the supernova is highlighted with crosshairs in the inset, appearing just to the left of a bright foreground star in our Milky Way Galaxy.  This supernova is currently thought to be of Type IIb, a stellar-core-collapse supernova, and is of high interest because it occurred so nearby and because it is being seen through a known dust filament. Current and future observations of this supernova may give us new clues about the fates of massive stars and how some elements found on our Earth were formed.",
  "hdurl": "http://apod.nasa.gov/apod/image/1602/CenAsupernova2_hubble_960.jpg",
  "media_type": "image",
  "service_version": "v1",
  "title": "A Supernova through Galaxy Dust",
  "url": "http://apod.nasa.gov/apod/image/1602/CenAsupernova2_hubble_960.jpg"
},

{
  "date": "2016-02-21",
  "explanation": "What's lighting up the Cigar Galaxy?  M82, as this irregular galaxy is also known, was stirred up by a recent pass near large spiral galaxy M81.  This doesn't fully explain the source of the red-glowing outwardly expanding gas, however.  Evidence indicates that this gas is being driven out by the combined emerging particle winds of many stars, together creating a galactic superwind.  The featured photographic mosaic highlights a specific color of red light strongly emitted by ionized hydrogen gas, showing detailed filaments of this gas.  The filaments extend for over 10,000 light years. The 12-million light-year distant Cigar Galaxy is the brightest galaxy in the sky in infrared light, and can be seen in visible light with a small telescope towards the constellation of the Great Bear (Ursa Major).",
  "hdurl": "http://apod.nasa.gov/apod/image/1602/m82_hubble_3000.jpg",
  "media_type": "image",
  "service_version": "v1",
  "title": "M82: Galaxy with a Supergalactic Wind",
  "url": "http://apod.nasa.gov/apod/image/1602/m82_hubble_960.jpg"
},

{
  "date": "2016-02-20",
  "explanation": "Want to take a relaxing interstellar vacation? Consider visiting Kepler-16b, a world in a binary star system. In fact Kepler-16b is the first discovered circumbinary planet. It was detected in a wide 229 day orbit around a close pair of cool, low-mass stars some 200 light-years away. The parent stars eclipse one another in their orbits, observed as a dimming of starlight. But Kepler-16b itself was discovered by following the additional very slight dimming produced during its transits. Like sci-fi planet Tatooine of Star Wars fame, two suns would set over its horizon. Still, Kepler 16b is probably not a Tatooine-like terrestrial desert world. Instead, Kepler 16b is thought to be a cold, uninhabitable planet with about the mass of Saturn and a gaseous surface ... so plan to dress accordingly. Or, choose another Visions of the Future vacation destination.",
  "hdurl": "http://apod.nasa.gov/apod/image/1602/Kepler16b_2600c.jpg",
  "media_type": "image",
  "service_version": "v1",
  "title": "Where Your Shadow Has Company",
  "url": "http://apod.nasa.gov/apod/image/1602/Kepler16b_1024c.jpg"
},

{
  "copyright": "Eric Coles",
  "date": "2016-02-19",
  "explanation": "Magnificent island universe NGC 2403 stands within the boundaries of the long-necked constellation Camelopardalis. Some 10 million light-years distant and about 50,000 light-years across, the spiral galaxy also seems to have more than its fair share of giant star forming HII regions, marked by the telltale reddish glow of atomic hydrogen gas. The giant HII regions are energized by clusters of hot, massive stars that explode as bright supernovae at the end of their short and furious lives. A member of the M81 group of galaxies, NGC 2403 closely resembles another galaxy with an abundance of star forming regions that lies within our own local galaxy group, M33 the Triangulum Galaxy. Spiky in appearance, bright stars in this colorful galaxy portrait of NGC 2403 are in the foreground, within our own Milky Way.",
  "hdurl": "http://apod.nasa.gov/apod/image/1602/NGC2403HaLRGBColesHelm2048.jpg",
  "media_type": "image",
  "service_version": "v1",
  "title": "NGC 2403 in Camelopardalis",
  "url": "http://apod.nasa.gov/apod/image/1602/NGC2403HaLRGBColesHelm1024.jpg"
},
{
  "copyright": "F. Scott Porter",
  "date": "2016-02-18",
  "explanation": "On February 17 at 5:45pm JST this H-IIA rocket blasted skyward from JAXA's Tanegashima Space Center located off the southern coast of Japan, planet Earth. Onboard was the ASTRO-H X-ray astronomy satellite, now in orbit. Designed to explore the extreme cosmos from black holes to massive galaxy clusters, the satellite observatory is equipped with four cutting-edge X-ray telescopes and instruments sensitive to photon energies from 300 to 600,000 electron volts. By comparison, visible light photon energies are 2 to 3 electron volts. Following a tradition of renaming satellites after their successful launch, ASTRO-H has been newly dubbed \"Hitomi\", inspired by an ancient legend of dragons. Hitomi means \"the pupil of the eye\".",
  "hdurl": "http://apod.nasa.gov/apod/image/1602/IMG_0193PorterAstroH.jpg",
  "media_type": "image",
  "service_version": "v1",
  "title": "Hitomi Launches",
  "url": "http://apod.nasa.gov/apod/image/1602/IMG_0193PorterAstroH_1024.jpg"
},
{
  "date": "2016-02-17",
  "explanation": "What strange world is this? Earth. In the foreground of the featured image are the Pinnacles, unusual rock spires in Nambung National Park in Western Australia. Made of ancient sea shells (limestone), how these human-sized picturesque spires formed remains unknown.  In the background, just past the end of the central Pinnacle, is a bright crescent Moon. The eerie glow around the Moon is mostly zodiacal light, sunlight reflected by dust grains orbiting between the planets in the Solar System. Arching across the top is the central band of our Milky Way Galaxy. Many famous stars and nebula are also visible in the background night sky. The featured 29-panel panorama was taken and composed last September after detailed planning that involved the Moon, the rock spires, and their corresponding shadows. Even so, the strong zodiacal light was a pleasant surprise.   Almost Hyperspace: Random APOD Generator",
  "hdurl": "http://apod.nasa.gov/apod/image/1602/PinnaclesGalaxy_Goh_2400.jpg",
  "media_type": "image",
  "service_version": "v1",
  "title": "Milky Way over the Pinnacles in Australia",
  "url": "http://apod.nasa.gov/apod/image/1602/PinnaclesGalaxy_Goh_1080.jpg"
},
{
  "date": "2016-02-16",
  "explanation": "Massive star IRS 4 is beginning to spread its wings.  Born only about 100,000 years ago, material streaming out from this newborn star has formed the nebula dubbed Sharpless 2-106 Nebula (S106), featured here.  A large disk of dust and gas orbiting Infrared Source 4 (IRS 4), visible in brown near the image center, gives the nebula an hourglass or butterfly shape.  S106 gas near IRS 4 acts as an emission nebula as it emits light after being ionized, while dust far from IRS 4 reflects light from the central star and so acts as a reflection nebula.  Detailed inspection of a recent infrared image of S106  reveal hundreds of low-mass brown dwarf stars lurking in the nebula's gas.  S106 spans about 2 light-years and lies about 2000 light-years away toward the  constellation of the Swan (Cygnus).",
  "hdurl": "http://apod.nasa.gov/apod/image/1602/S106_Pimenta_1824.jpg",
  "media_type": "image",
  "service_version": "v1",
  "title": "Star Forming Region S106",
  "url": "http://apod.nasa.gov/apod/image/1602/S106_Pimenta_960.jpg"
},
{
  "date": "2016-02-15",
  "explanation": "What caused this unusual light rock formation on Mars?  Intrigued by the possibility that they could be salt deposits left over as an ancient lakebed dried-up, detailed studies of these fingers now indicate a more mundane possibility: volcanic ash.  Studying the exact color of the formation indicated the possible volcanic origin.  The light material appears to have eroded away from surrounding area, indicating a very low-density substance. The stark contrast between the rocks and the surrounding sand is compounded by the unusual darkness of the sand.  The featured picture was taken with the Thermal Emission Imaging System on the Mars Odyssey, the longest serving  spacecraft currently orbiting Mars.  The image spans about 10 kilometers inside a larger crater.",
  "hdurl": "http://apod.nasa.gov/apod/image/1602/WhiteRock_MarsOdyssey_1238.jpg",
  "media_type": "image",
  "service_version": "v1",
  "title": "White Rock Fingers on Mars",
  "url": "http://apod.nasa.gov/apod/image/1602/WhiteRock_MarsOdyssey_960.jpg"
},
{
  "copyright": "Michael Kunze",
  "date": "2016-02-14",
  "explanation": "Can a cloud love a mountain?  Perhaps not, but on a Valentine's Day like today, one might be prone to seeing heart-shaped symbols where they don't actually exist.  A fleeting pareidolia, the featured heart was really a lenticular cloud that appeared one morning last July above Mount Cook National Park in New Zealand. A companion video shows the lenticular cloud was mostly stationary in the sky but shifted and vibrated with surrounding winds.  The cloud's red color was caused by the Sun rising off the frame to the right.  Lenticular clouds are somewhat rare but can form in air that passes over a mountain. Then, vertical eddies may form where rising air cools past the dew point causing water carried by the air to condense into droplets. Unfortunately, this amazing sight made the fascinated videographer late for breakfast.   Follow APOD on: Facebook,  Google Plus, or Twitter",
  "hdurl": "http://apod.nasa.gov/apod/image/1602/HeartCloud_Kunze_4650.jpg",
  "media_type": "image",
  "service_version": "v1",
  "title": "A Heart Shaped Lenticular Cloud",
  "url": "http://apod.nasa.gov/apod/image/1602/HeartCloud_Kunze_960.jpg"
},
{
  "date": "2016-02-13",
  "explanation": "Tracks lead to a small robot perched near the top of this bright little planet. Of course, the planet is really the Moon. The robot is the desk-sized Yutu rover, leaving its looming Chang'e 3 lander after a after a mid-December 2013 touch down in the northern Mare Imbrium. The little planet projection is a digitally warped and stitched mosaic of images from the lander's terrain camera covering 360 by 180 degrees. Ultimately traveling over 100 meters, Yutu came to a halt in January 2014. The lander's instruments are still working though, after more than two years on the lunar surface. Meanwhile, an interactive panoramic version of this little planet is available here.",
  "hdurl": "http://apod.nasa.gov/apod/image/1602/lunar-panorama-change-3-lander-2013-12-17.jpg",
  "media_type": "image",
  "service_version": "v1",
  "title": "Yutu on a Little Planet",
  "url": "http://apod.nasa.gov/apod/image/1602/lunar-panorama-change-3-lander-2013-12-17re.jpg"
},

{
  "date": "2016-02-11",
  "explanation": "Gravitational radiation has been directly detected.  The first-ever detection was made by both facilities of the Laser Interferometer Gravitational-Wave Observatory (LIGO) in Washington and Louisiana simultaneously last September. After numerous consistency checks, the resulting 5-sigma discovery was published today. The measured gravitational waves match those expected from two large black holes merging after a death spiral in a distant galaxy, with the resulting new black hole momentarily vibrating in a rapid ringdown.  A phenomenon predicted by Einstein, the historic discovery confirms a cornerstone of humanity's understanding of gravity and basic physics.  It is also the most direct detection of black holes ever.  The featured illustration depicts the two merging black holes with the signal strength of the two detectors over 0.3 seconds superimposed across the bottom. Expected future detections by Advanced LIGO and other gravitational wave detectors may not only confirm the spectacular nature of this measurement but hold tremendous promise of giving humanity a new way to see and explore our universe.",
  "hdurl": "http://apod.nasa.gov/apod/image/1602/BHmerger_LIGO_3600.jpg",
  "media_type": "image",
  "service_version": "v1",
  "title": "LIGO Detects Gravitational Waves from Merging Black Holes",
  "url": "http://apod.nasa.gov/apod/image/1602/BHmerger_LIGO_960.jpg"
},
{
  "copyright": "CEDIC Team",
  "date": "2016-02-10",
  "explanation": "Large galaxies grow by eating small ones. Even our own galaxy practices galactic cannibalism, absorbing small galaxies that get too close and are captured by the Milky Way's gravity. In fact, the practice is common in the universe and illustrated by this striking pair of interacting galaxies from the banks of the southern constellation Eridanus, The River. Located over 50 million light years away, the large, distorted spiral NGC 1532 is seen locked in a gravitational struggle with dwarf galaxy NGC 1531 (right of center), a struggle the smaller galaxy will eventually lose. Seen edge-on, spiral NGC 1532 spans about 100,000 light-years. Nicely detailed in this sharp image, the NGC 1532/1531 pair is thought to be similar to the well-studied system of face-on spiral and small companion known as M51.",
  "hdurl": "http://apod.nasa.gov/apod/image/1602/N1532_LRGB_50_finishCedic.jpg",
  "media_type": "image",
  "service_version": "v1",
  "title": "Galaxies in the River",
  "url": "http://apod.nasa.gov/apod/image/1602/N1532_LRGB_50_finishCedic1024.jpg"
}
]